﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LumtuRadio
{
    public class Radio
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Genre { get; set; }
        public string  Web { get; set; }

    }
}
