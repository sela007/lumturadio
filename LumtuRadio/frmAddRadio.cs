﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LumtuRadio
{
    public partial class frmAddRadio : Form
    {
        public frmAddRadio()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;

            this.Close();

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtName.Text.Trim() == "")
            {
                MessageBox.Show("Please enter name"); return;
            }
            if (txtUrl.Text.Trim() == "")
            {
                MessageBox.Show("Please enter Url"); return;
            }
            this.Close();
            this.DialogResult = System.Windows.Forms.DialogResult.OK;

        }
    }
}
