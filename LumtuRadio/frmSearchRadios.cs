﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LumtuRadio
{
    public partial class frmSearchRadios : Form
    {

        Settings settings;
        public delegate void delPlay(Radio r);
        private delPlay Play;
        public frmSearchRadios(Settings sett, delPlay p)
        {
            InitializeComponent();
            settings = sett;
            Play = p;
        }


        public Radio SelectedRadio { get; set; }
        private void frmSearchRadios_Load(object sender, EventArgs e)
        {
            RadioManager.SearchCompleted += RadioManager_SearchCompleted;
            txtSearch.GotFocus += txtSearch_GotFocus;
        }

        void txtSearch_GotFocus(object sender, EventArgs e)
        {
            txtSearch.SelectAll();
        }

        void RadioManager_SearchCompleted(object sender, RadiosEventArgs e)
        {
            dgRadios.Rows.Clear();
            foreach(Radio r in RadioManager.Radios ){
                dgRadios.Rows.Add(r.Name, r.Genre, r.Web, r.Url);

            }
            pb1.Style = ProgressBarStyle.Blocks;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            pb1.Style = ProgressBarStyle.Marquee;

            RadioManager.Search(txtSearch.Text);

        }

        
        private void dgRadios_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Player.Play(SelectedRadio);
            Play(SelectedRadio);

        }

        private void dgRadios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgRadios_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SelectedRadio = GetRadioFromRow(dgRadios.SelectedRows[0]);
        }

        private Radio GetRadioFromRow(DataGridViewRow dr)
        {
            var r = new Radio();
            r.Name = dr.Cells[0].Value.ToString();
            r.Genre = dr.Cells[1].Value.ToString();
            r.Web = dr.Cells[2].Value.ToString();
            r.Url = dr.Cells[3].Value.ToString();
            return r;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
           
        }

        private void btnAddToFavorites_Click(object sender, EventArgs e)
        {
            if(SelectedRadio !=null)
            settings.FavoriteRadios.Add(SelectedRadio);
            RefreshFavorites();
        }

        public void RefreshBookmarks()
        {
            foreach (Bookmark b in settings.Bookmarks)
            {
                AddBookmark(b);
            }
        }

        public void RefreshFavorites()
        {
            dgFavorties.Rows.Clear();
            foreach(var r in settings.FavoriteRadios){
                dgFavorties.Rows.Add(r.Name,r.Genre, r.Web,r.Url );
            }

        }
        public void AddBookmark(Bookmark b)
        {
            dgBookmarks.Rows.Add(b.Name, b.Radio, b.DateAdded);
        }

        public void LoadHistory()
        {
            foreach (Bookmark b in settings.History)
            {
                AddToHistory(b);
            }
        }

        public void AddToHistory(Bookmark b)
        {
            dgHistory.Rows.Insert (0, b.Name, b.Radio, b.DateAdded);
        }

        private void frmSearchRadios_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void dgFavorties_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SelectedRadio = GetRadioFromRow(dgFavorties.SelectedRows[0]);
        }

        private void btnRemoveFromFavorties_Click(object sender, EventArgs e)
        {
            if (dgFavorties.SelectedRows == null) return;
            settings.FavoriteRadios.RemoveAt(dgFavorties.SelectedRows[0].Index);
            dgFavorties.Rows.Remove(dgFavorties.SelectedRows[0]);

        }

        private void txtSearch_Click(object sender, EventArgs e)
        {
            txtSearch.SelectAll();
        }

        private void btnSearchGoogle_Click(object sender, EventArgs e)
        {
            SearchGoogle(dgBookmarks.SelectedRows[0].Cells[0].Value.ToString());
        }

        private void btnSearchYoutube_Click(object sender, EventArgs e)
        {
            SearchYoutube(dgBookmarks.SelectedRows[0].Cells[0].Value.ToString());
        }



        private void SearchGoogle(string strSearch)
        {
            System.Diagnostics.Process.Start(@"http://www.google.com/search?q=" + strSearch + " mp3" );
        }
        private void SearchYoutube(string strSearch)
        {
            System.Diagnostics.Process.Start(@"https://www.youtube.com/results?search_query=" + strSearch);
        }

        private void btnGoogle2_Click(object sender, EventArgs e)
        {
            SearchGoogle(dgHistory.SelectedRows[0].Cells[0].Value.ToString());
        }

        private void btnYoutube2_Click(object sender, EventArgs e)
        {
            SearchYoutube(dgHistory.SelectedRows[0].Cells[0].Value.ToString());
        }
        
    }
}
