﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace LumtuRadio
{
    public class Settings
    {
        public List<EQPreset> EQPresets;
        public int Volume { get; set; }
        public List<Radio> FavoriteRadios { get; set; }
        public List<Bookmark> Bookmarks { get; set; }
        public List<Bookmark> History { get; set; }
        public Color Color1 { get; set; }
        public Color Color2 { get; set; }
        public Color Color3 { get; set; }
        public Color Color4 { get; set; }
        public Color Color5 { get; set; }
        public Radio CurrentRadio { get; set; }
        public Point FormPosition { get; set; }
        public bool StayOnTop { get; set; }
        public int PrimaryScreen { get; set; }

        public Settings()
        {
            FavoriteRadios = new List<Radio>();
            Bookmarks = new List<Bookmark>();
            History = new List<Bookmark>();
            EQPresets = new List<EQPreset>();
            Volume = 61;
            CurrentRadio = new Radio();
            FormPosition = new Point(100, 100);
            PrimaryScreen = 0;
        }


        public void Save()
        {
            //settings sett = new settings();
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(Settings));
            string fPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "settings.dat"); 
          
            System.IO.StreamWriter file = new System.IO.StreamWriter(fPath);
            writer.Serialize(file, this);
            file.Close();
        }

        public static Settings Load()
        {
            string fPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "settings.dat"); 
            if (System.IO.File.Exists(fPath))
            {
                //settings sett = new settings();
                System.Xml.Serialization.XmlSerializer reader =
                    new System.Xml.Serialization.XmlSerializer(typeof(Settings));
                using (System.IO.StreamReader file = new System.IO.StreamReader(
                    fPath))
                {
                    return (Settings)reader.Deserialize(file);
                }
            }
            else
            { return new Settings(); }
        }

        public void AddToHistory(Bookmark b)
        {
            History.Insert(0, b);
            //spremaj samo zadnjih tisuću zapisa
            if (History.Count  == 1000) History.RemoveAt(999);
        }

    }

    public class Bookmark
    {
        public string Name { get; set; }
        public string Radio { get; set; }
        public DateTime  DateAdded { get; set; }
    }

}
