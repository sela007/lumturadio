﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Un4seen;
using Un4seen.Bass;
using System.Windows.Forms;
using System.Threading;
using System.Drawing;
using System.ComponentModel;


namespace LumtuRadio
{
    public static partial class Player
    {
        static Player()
        {
            BassNet.Registration("sela007@gmail.com", "2X2823717152222");
            if (Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT,IntPtr.Zero ) == false)
            {
               MessageBox.Show("BASS Init Error!", "Lumtu radio");
            }

            //ucitavanje bass_fx.dll
            Un4seen.Bass.AddOn.Fx.BassFx.BASS_FX_GetVersion();
            CurrentStream = new StreamInfo();
            Crossfade = true;
            ThreadCrossfadeIn = new Thread(CrossfadeInStart);
            ThreadCrossfadeOut = new Thread(CrossfadeOutStart);
            ChannelsToDispose = new List<int>();
            SpectrumHighQuality = true;
            SpectrumLineDistance = 1;
            SpectrumLineWidth = 1;
            SpectrumColor1 = Color.DarkGray;
            SpectrumColor2 = Color.Gray;
            SpectrumColor3 = Color.Yellow;
            SpectrumBackgroundColor = Color.Transparent;
            SpectrumMode = SpectrumType.Default;
            EQBandValue = new int[10];
            CrossfadeInSpeed = 30;
            CrossfadeOutSpeed = 30;
            StartBeforeEnd = 0;
            ReloadStreamWorker();
            ReloadStreamInfoWorker();

        }

        private static void ReloadStreamWorker()
        {
            LoadStreamWorker = new BackgroundWorker();
            LoadStreamWorker.WorkerSupportsCancellation = true;
            LoadStreamWorker.DoWork += LoadStreamStart;
            LoadStreamWorker.RunWorkerCompleted += LoadStreamCompleted;
        }
        private static void LoadStreamStart(object sender, DoWorkEventArgs e)
        {
            CurrentStream = new StreamInfo();
            BackgroundWorker worker = sender as BackgroundWorker;
            int i = Bass.BASS_StreamCreateURL(CurrentRadio.Url.ToString(), 0, BASSFlag.BASS_SAMPLE_FX, null, IntPtr.Zero);
            //ako je u vrijeme dok se učitava radio pokrenut drugi radio onda očisti memoriju i izađi
            if (worker.CancellationPending)
            {
                e.Cancel = true;
                Bass.BASS_StreamFree(i);
                //ako smo došli do ovdje znaći da LoadStreamWorker više 
                //nije worker (sender) i više se neče koristiti
                worker.Dispose();
                return;
            }
            CurrentChannel = i;
        }
        private static void LoadStreamCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (CurrentChannel == 0) { State = PlayerState.ServerUnreachable; }
            else
            { Set_EQ(CurrentChannel); Play(); }
        }

        private static void ReloadStreamInfoWorker()
        {
            GetCurrrentStreamInfoWorker = new BackgroundWorker();
            GetCurrrentStreamInfoWorker.WorkerSupportsCancellation = true;
            GetCurrrentStreamInfoWorker.WorkerReportsProgress = true;
            GetCurrrentStreamInfoWorker.DoWork += GetStreamInfoDoWork;
            GetCurrrentStreamInfoWorker.RunWorkerCompleted += GetStreamInfoCompleted;
            GetCurrrentStreamInfoWorker.ProgressChanged += GetStreamInfoProgressChanged;
        }

        static void GetStreamInfoProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //zove se prije GetStreamInfoCompleted   
            //i podize se StreamInfoChanged ako je detektirana nova stvar


            StreamInfo value=  e.UserState as StreamInfo ;
            PlayerEventArgs args = new PlayerEventArgs() ;
            args.StreamAuthor = value.CurrentSongAuthor;
            args.StreamTitle = value.CurrentSongTitle;
            if (CurrentStream.CurrentSongAuthor != value.CurrentSongAuthor || CurrentStream.CurrentSongTitle != value.CurrentSongTitle )
            {
                CurrentStream = value;
                if (StreamInfoChanged != null) StreamInfoChanged(args);
            }
        }

       
        public static void UpdateCurrentStreamInfo()
        {
            //ucitavanje autora i naslova pjesme koja trenutno svira na radiju. 
            // sprema u CurentStreamTitle i CurrentStreamAutor i zove event StreamInfoCompleted
            //if (State != PlayerState.Streaming)
            //{
            //    CurrentStream = new StreamInfo();
            //    StreamInfoCompleted(new PlayerEventArgs());
            //    return;
            //}

            if (CurrentRadio == null) return;
            // ako je vec u tijeku ucitavanje
            if (GetCurrrentStreamInfoWorker.IsBusy)
            {
                //postavljanje bw.CancellationPending prop = true
                GetCurrrentStreamInfoWorker.CancelAsync();
                //pošto je još u tijeku  učitavanje prethodnog streama potrebno je napraviti novi bw objekt da bi
                //se odma počelo sa učitavanjem novog streama 
                ReloadStreamInfoWorker();
            }
            GetCurrrentStreamInfoWorker.RunWorkerAsync();
        }

        private static void GetStreamInfoDoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Un4seen.Bass.AddOn.Tags.TAG_INFO tagInfo = new Un4seen.Bass.AddOn.Tags.TAG_INFO(CurrentRadio.Url);
                BackgroundWorker worker = sender as BackgroundWorker;
                if (Un4seen.Bass.AddOn.Tags.BassTags.BASS_TAG_GetFromURL(Player.CurrentChannel, tagInfo))
                {


                    StreamInfo info = new StreamInfo();
                    info.CurrentSongAuthor = tagInfo.title;
                    info.CurrentSongTitle = tagInfo.artist;
                    //CurrentStream = info;                    
                    ProgressChangedEventArgs args = new ProgressChangedEventArgs(0, info);
                    worker.ReportProgress(0, info);
                }
                 
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    //ako smo došli do ovdje znaći da LoadStreamWorker više 
                    //nije worker (sender) i više se neče koristiti
                    worker.Dispose();
                    return;
                }
                 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lumtu Radio");
            }
        }
        public static event PlayerEventsDelegate StreamInfoChanged;
        public static event PlayerEventsDelegate StreamInfoCompleted;
        private static void GetStreamInfoCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            
            PlayerEventArgs args = new PlayerEventArgs();
            args.StreamAuthor = CurrentStream.CurrentSongAuthor;
            args.StreamTitle = CurrentStream.CurrentSongTitle;
            args.State = State;
            args.Filepath = CurrentRadio.Url;
            if(StreamInfoCompleted !=null) StreamInfoCompleted(args);
         
        }
        
        public delegate void PlayerEventsDelegate(PlayerEventArgs e);
        public static event PlayerEventsDelegate StateChanged;
        

        public  enum PlayerState {Playing,Stopped,Paused,Streaming,Loading,ServerUnreachable,FileNotFound}
        private static PlayerState _State;
        public static PlayerState State {

            get { return _State; }

            set { _State = value;
            PlayerEventArgs args = new PlayerEventArgs { State = value };
            if(StateChanged !=null) StateChanged(args);
            } 
        }

        private static int CurrentChannel;
        private static int PreviousChannel;

        public static int StartBeforeEnd { get; set; }

        private static int CrossfadeInSpeed { get; set; } // default=30 >> milisekundi - svakih x mili sekundi povecava za 0.01 do max=1
        private static int CrossfadeOutSpeed { get; set; } 

        private static Thread ThreadCrossfadeIn;
        private static Thread ThreadCrossfadeOut;

         
        private static void CrossfadeStart()
        {
            CurrentChannelVol  = 0;
            PreviousChannelVol = 100;

            if (!ThreadCrossfadeIn.IsAlive)
            {
                ThreadCrossfadeIn = new Thread(CrossfadeInStart);
                //pozovi _CrossfadeInStart()
                ThreadCrossfadeIn.Start();
            }

            if (PreviousChannel != 0)
            {
                if (!ThreadCrossfadeOut.IsAlive)
                {
                    ThreadCrossfadeOut = new Thread(CrossfadeOutStart);
                    //pozovi _CrossfadeStart()
                    ThreadCrossfadeOut.Start();
                }
            }
        }


        private delegate void delSub();
        private static void CrossfadeInStart()
        {
            delSub incrase = new delSub(CrossfadeIn);
            while (CurrentChannelVol < 100)
            {
                Thread.Sleep(CrossfadeInSpeed);
                incrase();
            }
        }

        private static void CrossfadeIn()
        {
            CurrentChannelVol += 1;
        }

        private static void CrossfadeOutStart()
        {
            delSub incrase = new delSub(CrossfadeOut);
            while (PreviousChannelVol > 0)
            {
                Thread.Sleep(CrossfadeOutSpeed);
                incrase();
            }

            foreach (int chan in ChannelsToDispose)
            {
                Bass.BASS_StreamFree(chan);
            }
        }

        private static void CrossfadeOut()
        {
            PreviousChannelVol -= 1;
        }

        private static int _CurrentChannelVol;
        private static int CurrentChannelVol {
            get {
                return _CurrentChannelVol;
                }
            set {
                if (value > 100) value = 100;
                if (value < 0) value = 0;
                _CurrentChannelVol = value;
                Bass.BASS_ChannelSetAttribute(CurrentChannel, BASSAttribute.BASS_ATTRIB_VOL,Convert.ToSingle( value) / Convert.ToSingle( 100));
                } 
        }

        private static int _PreviousChannelVol;
        private static int PreviousChannelVol
        {
            get
            {
                return _PreviousChannelVol;
            }
            set
            {
                if (value > 100) value = 100;
                if (value < 0) value = 0;
                _PreviousChannelVol = value;
                foreach(int chan in ChannelsToDispose )
                Bass.BASS_ChannelSetAttribute(chan, BASSAttribute.BASS_ATTRIB_VOL, Convert.ToSingle(value) / Convert.ToSingle(100));
            }
        }

        private static List<int> ChannelsToDispose;

        private static bool IsUriFormat()
        {
           if(CurrentRadio !=null)
           return CurrentRadio.Url.ToUpper().StartsWith("HTTP://");
           return false;
        }
         
        public static event PlayerEventsDelegate FileChanged;

        public static BackgroundWorker LoadStreamWorker;
        public static BackgroundWorker GetCurrrentStreamInfoWorker;


        public static void Play(Radio radio)
        {

            CurrentRadio = new Radio { Name = radio.Name, Genre = radio.Genre, Web = radio.Web, Url = radio.Url };
            CurrentStream = new StreamInfo();
      
            //memorirajnje trenutnog kanala radi crossfadea
            if (CurrentChannel != 0) {
                 
                PreviousChannel = CurrentChannel;
                 
                ChannelsToDispose.Add(PreviousChannel);
            }

            //if (radiop) // ucitavanje radija
            //{
                State = PlayerState.Loading;
                //CurrentStream = new StreamInfo { StreamTitle = radio.Name };

                //oslobodi memoriju od prethodnog fajla ako postoji
                if (PreviousChannel != 0)
                {
                    Bass.BASS_ChannelStop(PreviousChannel);
                    Bass.BASS_StreamFree(PreviousChannel);
                }
                
                // postavi vol na 100 odmah, pošto nije omogučen krosfejd kada svira radio
                CurrentChannelVol = 100;
                
                // ako je vec u tijeku ucitavanje
                if (LoadStreamWorker.IsBusy)
                {   
                    //postavljanje bw.CancellationPending prop = true
                    LoadStreamWorker.CancelAsync();
                    //pošto je još u tijeku  učitavanje prethodnog streama potrebno je napraviti novi bw objekt da bi
                    //se odma počelo sa učitavanjem novog streama 
                    ReloadStreamWorker();
                }

                //pokrece void LoadStreamStart
                LoadStreamWorker.RunWorkerAsync(); 
                
            //}
            //else //mp3
            //{
            //    CurrentChannel = Bass.BASS_StreamCreateFile(path, 0, 0, BASSFlag.BASS_SAMPLE_FLOAT);
            //    if (CurrentChannel == 0) { State = PlayerState.FileNotFound; }
            //    else
            //    {
            //        CurrentRadio = Files.GetFullInfo(path, CurrentChannel);
            //        State = PlayerState.Playing;
            //        Set_EQ(CurrentChannel);
            //    }
            //}


            if (State == PlayerState.FileNotFound) 
            {
                Bass.BASS_ChannelStop(PreviousChannel);
                Bass.BASS_StreamFree(PreviousChannel);
                CurrentRadio = null;
            
            }else
            {
                if (Crossfade)
                {

                    Bass.BASS_ChannelPlay(CurrentChannel, false);
                    CrossfadeStart();
                }
                else
                {
                    //pošto je crossfade=false podesi glasnocu kanala na max odmah
                    CurrentChannelVol = 100;
                    //ako je crossfade disabled raspusti prethodnu stvar odmah
                    Bass.BASS_ChannelStop(PreviousChannel);
                    Bass.BASS_StreamFree(PreviousChannel);

                    Bass.BASS_ChannelPlay(CurrentChannel, false);
                }
            }


        }


        public static void Play()
        {
            Bass.BASS_ChannelPlay(CurrentChannel, false);

            if (IsUriFormat()) { State = PlayerState.Streaming; }
            else
            {
                State = PlayerState.Playing;
            }
        }



      

        public delegate void delChangeState (PlayerState state);
        private static void ChangeState(PlayerState state)
        {
            State = state;
        }
        public static void Stop()
        {
            Bass.BASS_ChannelStop(CurrentChannel);
            Bass.BASS_StreamFree(CurrentChannel);
            State = PlayerState.Stopped;
        }

        public static void Pause()
        {
            Bass.BASS_ChannelPause(CurrentChannel);
            State = PlayerState.Paused;
        }

        public static void PlayPause()
        {
            switch (State)
            {
                case PlayerState.Paused:
                    Play();
                    break;
                case PlayerState.Playing:
                    Pause();
                    break;
                case PlayerState.Streaming:
                    Pause();
                    break;
                case PlayerState.Stopped:
                    Play(CurrentRadio );
                    break;
            }
 
        }



        public static bool Crossfade { get; set; }

        public static int CurrentPosition
        {
            get
            {

                return Convert.ToInt32(Bass.BASS_ChannelBytes2Seconds(CurrentChannel, Bass.BASS_ChannelGetPosition(CurrentChannel)));
                
            }

            set
            {
                Bass.BASS_ChannelSetPosition(CurrentChannel, Bass.BASS_ChannelSeconds2Bytes(CurrentChannel, value));
            }
        }

        public static int Volume {
            get
            {
                return Convert.ToInt16((Bass.BASS_GetConfig(BASSConfig.BASS_CONFIG_GVOL_STREAM) / 100));
            } 
            set {            
         
                int val = value * 100;
                Bass.BASS_SetConfig(BASSConfig.BASS_CONFIG_GVOL_STREAM, val);
            } 
        }


        private static Radio _CurrentRadio;
        public static Radio CurrentRadio
        {
            get { return _CurrentRadio; }
            set 
            {
                PlayerEventArgs args = new PlayerEventArgs();
                args.State = Player.State;

                _CurrentRadio = value;
                if(FileChanged !=null)
                        FileChanged(args);
                        return;
            }
        }

        private static StreamInfo _CurrentStream;
        public static StreamInfo CurrentStream
        {
            get { return _CurrentStream; }
            set
            {
          
                PlayerEventArgs args = new PlayerEventArgs();
                args.State = Player.State;
                args.StreamAuthor = value.CurrentSongAuthor;
                args.StreamTitle = value.StreamTitle;
                _CurrentStream = value;
                if (FileChanged != null)
                FileChanged(args);

                return;
            }
        }

        public static void ApplyPreset(EQPreset p)
        {
            for (int i = 0; i <= 9; i++)
            {
                EQBandValue[i] = p.Values[i];
                UpdateEQ(i);

            }

        }

  


    }

    public class PlayerEventArgs : EventArgs
    {
        public Player.PlayerState State { get; set; }
        public string Filepath { get; set; }
        public string StreamTitle { get; set; }
        public string StreamAuthor { get; set; }
    }

    public class StreamInfo : RadioInfo
    {
        public string StreamTitle { get; set; }
        public string CurrentSongTitle { get; set; }
        public string CurrentSongAuthor { get; set; }
    }


    public class EQPreset
    {
        public string Name { get; set; }
        public int[] Values = new int[10];
    }
}
