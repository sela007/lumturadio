﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Net;
using Newtonsoft;
using Newtonsoft.Json;
using System.IO;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ComponentModel;

namespace LumtuRadio
{
    [Serializable()]
    public static class RadioManager
    {
        private static string apiKey = "9dddd3d127a829374330e6c448c70e4ec0b3a49f";


        static RadioManager()
        {
            //Table = Func.CreateEmptyDataTable(typeof(tblRadios));
            ReloadSearchWorker();
            Radios = new List<Radio>();
        }


        public static List <Radio> Radios { get; set; }


        private static void ReloadSearchWorker()
        {
             
            SearchRadiosWorker = new BackgroundWorker();
            SearchRadiosWorker.WorkerSupportsCancellation = true;
            SearchRadiosWorker.DoWork += _GetRadios;
            SearchRadiosWorker.RunWorkerCompleted += SearchRadiosWorker_RunWorkerCompleted;
        }

        static void SearchRadiosWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            RadiosEventArgs args=new RadiosEventArgs ();
            SearchCompleted(null, args);
        }

       
        
        public static void Search(string strSearch)
        {
            if (SearchRadiosWorker.IsBusy)
            {
                //postavljanje bw.CancellationPending prop = true
                SearchRadiosWorker.CancelAsync();
                //pošto je još u tijeku  traženje potrebno je napraviti novi bw objekt da bi
                //se odma počelo sa novim traženjem 
                ReloadSearchWorker();
            }
            SearchRadiosWorker.RunWorkerAsync(strSearch);
        }
        private static void _GetRadios(object sender, DoWorkEventArgs e)
        {
            List<RadioInfo> radios = SearchRadios(e.Argument.ToString());
            Radios.Clear();
            foreach (RadioInfo info in radios)
            {
                string stream = "";
                if (info.streams.Count > 0) stream = info.streams[0].stream;

                Radios.Add(new Radio { Name = info.name,Genre="", Web = info.website, Url = stream });

                //Table.Rows.Add(info.name, "", info.website,  0,stream);
            }
        }


        private static BackgroundWorker SearchRadiosWorker;
        public delegate void RadiosEventsDelegate(object sender, RadiosEventArgs e);
        public static event RadiosEventsDelegate SearchCompleted;
       


        public static List<RadioInfo> SearchRadios(string strSearch)
        {
            List<RadioInfo> radios = new List<RadioInfo>();

            string str = string.Format("http://api.dirble.com/v2/search/{0}?page=1&per_page=20&token={1}", strSearch, apiKey);
            
            string strJSON = GetJSON(str);
            //Clipboard.SetText(strJSON);
            radios.AddRange(getRadiosFromJSON(strJSON));
            return radios;
        }




        public static TreeNode genreNode = new TreeNode("Genres");
        public static void UpdateGenres()
        {
            //genreNode.Nodes.Clear();
            //genreNode.Tag = "RADIOSGENRES";
            //genreNode.ImageIndex = 1;

            //string str = @"http://api.dirble.com/v1/primaryCategories/apikey\\/" + apiKey;
            //string strJSON = GetJSON(str);
            //Newtonsoft.Json.JsonTextReader jr = new Newtonsoft.Json.JsonTextReader(new StringReader(strJSON));
            //while ((jr.Read()))
            //{
            //    if ((jr.Value != null))
            //    {
            //        if (jr.Value.ToString().ToUpper() == "ID")
            //        {
            //            GenreInfo g = new GenreInfo();
            //            jr.Read();
            //            g.Id = jr.Value.ToString();
            //            jr.Read();
            //            jr.Read();
            //            g.Name = jr.Value.ToString();
            //            TreeNode n = new TreeNode();
            //            n.Text = g.Name;
            //            n.Tag = g.Id;
            //            n.ImageIndex = 1;
            //            n.SelectedImageIndex = 1;
            //            genreNode.Nodes.Add(n);
            //        }
            //    }
            //}
            //UpdateSubGenres();

        }

        public static void UpdateSubGenres()
        {
            foreach (TreeNode n in genreNode.Nodes)
            {
                UpdateGenreNode(n);
            }
        }
        private static void UpdateGenreNode(TreeNode genreNode)
        {
            //string str = @"http://api.dirble.com/v1/childCategories/apikey/" + apiKey + @"/primaryid/" + genreNode.Tag.ToString();
            //string strJSON = GetJSON(str);
            //Newtonsoft.Json.JsonTextReader jr = new Newtonsoft.Json.JsonTextReader(new StringReader(strJSON));
            //while ((jr.Read()))
            //{
            //    if ((jr.Value != null))
            //    {
            //        if (jr.Value.ToString().ToUpper() == "ID")
            //        {
            //            GenreInfo g = new GenreInfo();
            //            jr.Read();
            //            g.Id = jr.Value.ToString();
            //            jr.Read();
            //            jr.Read();
            //            g.Name = jr.Value.ToString();

            //            TreeNode n = new TreeNode();
            //            n.Text = g.Name;
            //            n.Tag = g.Id;
            //            n.ImageIndex = 1;
            //            n.SelectedImageIndex = 1;
            //            genreNode.Nodes.Add(n);

            //        }
            //    }
            //}

        }

        private static string GetJSON(string strUrl)
        {
            try
            {
                HttpWebRequest request = null;
                HttpWebResponse response = null;
                StreamReader reader = null;
                request = (HttpWebRequest)WebRequest.Create(strUrl);
                response = (HttpWebResponse)request.GetResponse();
                reader = new StreamReader(response.GetResponseStream());
                string strResponse = reader.ReadToEnd();
                return strResponse;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }


        }



        public  static List<RadioInfo> GetRadiosFromGenre(string strGenreId)
        {
            string str = "http://api.dirble.com/v1/stations/apikey/" + apiKey + "/id/" + strGenreId;
            string strJSON = GetJSON(str);
            return getRadiosFromJSON(strJSON);
        }


        public static List<RadioInfo> getRadiosFromJSON(string strJSON)
        {
             
            List<RadioInfo> SR = new List<RadioInfo>();
            SR = JsonConvert.DeserializeObject<List<RadioInfo>>(strJSON);
            return SR;
        }

        public static List<string> GetSongHistory(string radioID)
        {
            List<string> SR = new List<string>();
            string str = "http://api.dirble.com/v1/station/apikey/" + apiKey + "/id/" + radioID;
            string strJSON = GetJSON(str);
            bool sh = false;
            Newtonsoft.Json.JsonTextReader jr = new Newtonsoft.Json.JsonTextReader(new StringReader(strJSON));
            while ((jr.Read()))
            {
                if ((jr.Value != null))
                {
                    if (jr.Value.ToString() == "songhistory")
                        sh = true;
                    if (sh)
                    {
                        if (jr.Value.ToString() == "title")
                        {
                            jr.Read();
                            SR.Add(jr.Value.ToString());
                        }
                    }
                }
            }
            return SR;
        }
    }

    public class RadiosEventArgs : EventArgs
    {
         
    }

    public class Thumb
    {
        public object url { get; set; }
    }

    public class Image2
    {
        public object url { get; set; }
        public Thumb thumb { get; set; }
    }

    public class Image
    {
        public Image2 image { get; set; }
    }

    public class Stream
    {
        public int id { get; set; }
        public string stream { get; set; }
        public int bitrate { get; set; }
        public string content_type { get; set; }
        public int status { get; set; }
        public int Station_id { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public bool timedout { get; set; }
        public int emptycounter { get; set; }
    }

    public class Category
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public object urlid { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string slug { get; set; }
        public string ancestry { get; set; }
        public object position { get; set; }
    }

    public class StationSong
    {
        public string id { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public int week { get; set; }
        public int year { get; set; }
        public int station_id { get; set; }
        public object info { get; set; }
        public string date { get; set; }
    }

    public class RadioInfo
    {
        public int id { get; set; }
        public string name { get; set; }
        public int accepted { get; set; }
        public string added { get; set; }
        public string country { get; set; }
        public string description { get; set; }
        public Image image { get; set; }
        public string slug { get; set; }
        public string website { get; set; }
        public List<Stream> streams { get; set; }
        public List<Category> categories { get; set; }
        public List<StationSong> station_songs { get; set; }

        public string Rating { get; set; }
        public bool Favorite { get; set; }
    }

}
