﻿namespace LumtuRadio
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tViz = new System.Windows.Forms.Timer(this.components);
            this.cmFavorites = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.lblRadio = new System.Windows.Forms.Label();
            this.lblSong = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.cmSettings = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addRadioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookmarksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveToBottomRightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItmStayOnTop = new System.Windows.Forms.ToolStripMenuItem();
            this.pbVol = new System.Windows.Forms.PictureBox();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnEq = new System.Windows.Forms.Button();
            this.btnFavorites = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.btnBookmark = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.pbViz = new System.Windows.Forms.PictureBox();
            this.favoritesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchRadiosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.songHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbVol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbViz)).BeginInit();
            this.SuspendLayout();
            // 
            // tViz
            // 
            this.tViz.Enabled = true;
            this.tViz.Interval = 10;
            this.tViz.Tick += new System.EventHandler(this.tViz_Tick);
            // 
            // cmFavorites
            // 
            this.cmFavorites.Name = "cmFavorites";
            this.cmFavorites.Size = new System.Drawing.Size(61, 4);
            this.cmFavorites.Opening += new System.ComponentModel.CancelEventHandler(this.cmFavorites_Opening);
            // 
            // lblRadio
            // 
            this.lblRadio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRadio.Font = new System.Drawing.Font("Trebuchet MS", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblRadio.ForeColor = System.Drawing.Color.MintCream;
            this.lblRadio.Location = new System.Drawing.Point(47, 19);
            this.lblRadio.Name = "lblRadio";
            this.lblRadio.Size = new System.Drawing.Size(129, 16);
            this.lblRadio.TabIndex = 3;
            this.lblRadio.Text = "Lumtu radio";
            this.lblRadio.Click += new System.EventHandler(this.btnFavorites_Click);
            // 
            // lblSong
            // 
            this.lblSong.AutoSize = true;
            this.lblSong.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSong.ForeColor = System.Drawing.Color.MintCream;
            this.lblSong.Location = new System.Drawing.Point(47, 35);
            this.lblSong.Name = "lblSong";
            this.lblSong.Size = new System.Drawing.Size(0, 12);
            this.lblSong.TabIndex = 3;
            this.lblSong.TextChanged += new System.EventHandler(this.lblSong_TextChanged);
            this.lblSong.Click += new System.EventHandler(this.lblSong_Click);
            this.lblSong.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 4000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // cmSettings
            // 
            this.cmSettings.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addRadioToolStripMenuItem,
            this.searchRadiosToolStripMenuItem,
            this.favoritesToolStripMenuItem,
            this.bookmarksToolStripMenuItem,
            this.songHistoryToolStripMenuItem,
            this.toolStripMenuItem1,
            this.moveToBottomRightToolStripMenuItem,
            this.mnuItmStayOnTop,
            this.toolStripMenuItem2,
            this.exitToolStripMenuItem});
            this.cmSettings.Name = "cmSettings";
            this.cmSettings.Size = new System.Drawing.Size(176, 192);
            // 
            // addRadioToolStripMenuItem
            // 
            this.addRadioToolStripMenuItem.Name = "addRadioToolStripMenuItem";
            this.addRadioToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.addRadioToolStripMenuItem.Text = "Add radio";
            this.addRadioToolStripMenuItem.Click += new System.EventHandler(this.addRadioToolStripMenuItem_Click);
            // 
            // bookmarksToolStripMenuItem
            // 
            this.bookmarksToolStripMenuItem.Name = "bookmarksToolStripMenuItem";
            this.bookmarksToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.bookmarksToolStripMenuItem.Text = "Bookmarks";
            this.bookmarksToolStripMenuItem.Click += new System.EventHandler(this.bookmarksToolStripMenuItem_Click);
            // 
            // moveToBottomRightToolStripMenuItem
            // 
            this.moveToBottomRightToolStripMenuItem.Name = "moveToBottomRightToolStripMenuItem";
            this.moveToBottomRightToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.moveToBottomRightToolStripMenuItem.Text = "Move to bottom right";
            this.moveToBottomRightToolStripMenuItem.Click += new System.EventHandler(this.moveToBottomRightToolStripMenuItem_Click);
            // 
            // mnuItmStayOnTop
            // 
            this.mnuItmStayOnTop.Name = "mnuItmStayOnTop";
            this.mnuItmStayOnTop.Size = new System.Drawing.Size(175, 22);
            this.mnuItmStayOnTop.Text = "Stay on top";
            this.mnuItmStayOnTop.Click += new System.EventHandler(this.stayOnTopToolStripMenuItem_Click);
            // 
            // pbVol
            // 
            this.pbVol.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbVol.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbVol.Location = new System.Drawing.Point(182, 22);
            this.pbVol.Name = "pbVol";
            this.pbVol.Size = new System.Drawing.Size(100, 12);
            this.pbVol.TabIndex = 4;
            this.pbVol.TabStop = false;
            this.pbVol.Click += new System.EventHandler(this.pbVol_Click);
            this.pbVol.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbVol_MouseDown);
            this.pbVol.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbVol_MouseMove);
            // 
            // btnMinimize
            // 
            this.btnMinimize.AutoSize = true;
            this.btnMinimize.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.btnMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMinimize.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SteelBlue;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnMinimize.ForeColor = System.Drawing.Color.MintCream;
            this.btnMinimize.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimize.Image")));
            this.btnMinimize.Location = new System.Drawing.Point(279, -3);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(26, 22);
            this.btnMinimize.TabIndex = 0;
            this.btnMinimize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMinimize.UseVisualStyleBackColor = false;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.AutoSize = true;
            this.btnClose.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SteelBlue;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnClose.ForeColor = System.Drawing.Color.MintCream;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(303, -3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(26, 22);
            this.btnClose.TabIndex = 0;
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnEq
            // 
            this.btnEq.AutoSize = true;
            this.btnEq.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.btnEq.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEq.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnEq.FlatAppearance.BorderSize = 0;
            this.btnEq.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnEq.FlatAppearance.MouseOverBackColor = System.Drawing.Color.CornflowerBlue;
            this.btnEq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEq.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnEq.ForeColor = System.Drawing.Color.MintCream;
            this.btnEq.Image = ((System.Drawing.Image)(resources.GetObject("btnEq.Image")));
            this.btnEq.Location = new System.Drawing.Point(256, -3);
            this.btnEq.Name = "btnEq";
            this.btnEq.Size = new System.Drawing.Size(26, 22);
            this.btnEq.TabIndex = 0;
            this.btnEq.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEq.UseVisualStyleBackColor = false;
            this.btnEq.Click += new System.EventHandler(this.btnFavorites_Click);
            // 
            // btnFavorites
            // 
            this.btnFavorites.AutoSize = true;
            this.btnFavorites.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.btnFavorites.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFavorites.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnFavorites.FlatAppearance.BorderSize = 0;
            this.btnFavorites.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnFavorites.FlatAppearance.MouseOverBackColor = System.Drawing.Color.CornflowerBlue;
            this.btnFavorites.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFavorites.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnFavorites.ForeColor = System.Drawing.Color.MintCream;
            this.btnFavorites.Image = ((System.Drawing.Image)(resources.GetObject("btnFavorites.Image")));
            this.btnFavorites.Location = new System.Drawing.Point(52, -3);
            this.btnFavorites.Name = "btnFavorites";
            this.btnFavorites.Size = new System.Drawing.Size(26, 22);
            this.btnFavorites.TabIndex = 0;
            this.btnFavorites.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFavorites.UseVisualStyleBackColor = false;
            this.btnFavorites.Click += new System.EventHandler(this.btnFavorites_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.AutoSize = true;
            this.btnSearch.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.CornflowerBlue;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnSearch.ForeColor = System.Drawing.Color.MintCream;
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(26, -3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(26, 22);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnGetRadios_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.AutoSize = true;
            this.btnSettings.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.btnSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSettings.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnSettings.FlatAppearance.BorderSize = 0;
            this.btnSettings.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.CornflowerBlue;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnSettings.ForeColor = System.Drawing.Color.MintCream;
            this.btnSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnSettings.Image")));
            this.btnSettings.Location = new System.Drawing.Point(0, -3);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(26, 22);
            this.btnSettings.TabIndex = 0;
            this.btnSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // btnBookmark
            // 
            this.btnBookmark.BackColor = System.Drawing.Color.Transparent;
            this.btnBookmark.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnBookmark.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBookmark.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnBookmark.FlatAppearance.BorderSize = 0;
            this.btnBookmark.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnBookmark.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnBookmark.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBookmark.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnBookmark.ForeColor = System.Drawing.Color.MintCream;
            this.btnBookmark.Image = ((System.Drawing.Image)(resources.GetObject("btnBookmark.Image")));
            this.btnBookmark.Location = new System.Drawing.Point(288, 19);
            this.btnBookmark.Name = "btnBookmark";
            this.btnBookmark.Size = new System.Drawing.Size(41, 35);
            this.btnBookmark.TabIndex = 2;
            this.btnBookmark.TabStop = false;
            this.btnBookmark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBookmark.UseVisualStyleBackColor = false;
            this.btnBookmark.Click += new System.EventHandler(this.btnBookmark_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.BackColor = System.Drawing.Color.Transparent;
            this.btnPlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPlay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPlay.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnPlay.FlatAppearance.BorderSize = 0;
            this.btnPlay.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnPlay.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPlay.ForeColor = System.Drawing.Color.MintCream;
            this.btnPlay.Image = global::LumtuRadio.Properties.Resources.play_3_32;
            this.btnPlay.Location = new System.Drawing.Point(0, 19);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(41, 35);
            this.btnPlay.TabIndex = 2;
            this.btnPlay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPlay.UseVisualStyleBackColor = false;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // pbViz
            // 
            this.pbViz.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.pbViz.Location = new System.Drawing.Point(78, 0);
            this.pbViz.Name = "pbViz";
            this.pbViz.Size = new System.Drawing.Size(183, 19);
            this.pbViz.TabIndex = 1;
            this.pbViz.TabStop = false;
            this.pbViz.Click += new System.EventHandler(this.pbViz_Click);
            this.pbViz.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.pbViz.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbViz_MouseUp);
            // 
            // favoritesToolStripMenuItem
            // 
            this.favoritesToolStripMenuItem.Name = "favoritesToolStripMenuItem";
            this.favoritesToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.favoritesToolStripMenuItem.Text = "Favorites";
            this.favoritesToolStripMenuItem.Click += new System.EventHandler(this.favoritesToolStripMenuItem_Click);
            // 
            // searchRadiosToolStripMenuItem
            // 
            this.searchRadiosToolStripMenuItem.Name = "searchRadiosToolStripMenuItem";
            this.searchRadiosToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.searchRadiosToolStripMenuItem.Text = "Search radios";
            this.searchRadiosToolStripMenuItem.Click += new System.EventHandler(this.searchRadiosToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(172, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(172, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // songHistoryToolStripMenuItem
            // 
            this.songHistoryToolStripMenuItem.Name = "songHistoryToolStripMenuItem";
            this.songHistoryToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.songHistoryToolStripMenuItem.Text = "Song history";
            this.songHistoryToolStripMenuItem.Click += new System.EventHandler(this.songHistoryToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(329, 54);
            this.Controls.Add(this.pbVol);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnEq);
            this.Controls.Add(this.btnFavorites);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.lblRadio);
            this.Controls.Add(this.btnBookmark);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.pbViz);
            this.Controls.Add(this.lblSong);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "LumtuRadio";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.cmSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbVol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbViz)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.PictureBox pbViz;
        private System.Windows.Forms.Timer tViz;
        private System.Windows.Forms.ContextMenuStrip cmFavorites;
        private System.Windows.Forms.Label lblRadio;
        private System.Windows.Forms.Label lblSong;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnFavorites;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnBookmark;
        private System.Windows.Forms.Button btnEq;
        private System.Windows.Forms.PictureBox pbVol;
        private System.Windows.Forms.ContextMenuStrip cmSettings;
        private System.Windows.Forms.ToolStripMenuItem moveToBottomRightToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addRadioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bookmarksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuItmStayOnTop;
        private System.Windows.Forms.ToolStripMenuItem searchRadiosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem favoritesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem songHistoryToolStripMenuItem;
    }
}

