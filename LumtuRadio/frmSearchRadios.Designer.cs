﻿namespace LumtuRadio
{
    partial class frmSearchRadios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.dgRadios = new System.Windows.Forms.DataGridView();
            this.tc1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnAddToFavorites = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnSearchGoogle = new System.Windows.Forms.Button();
            this.btnSearchYoutube = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dgBookmarks = new System.Windows.Forms.DataGridView();
            this.SongName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cRadio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateAdded = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dgHistory = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgFavorties = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRemoveFromFavorties = new System.Windows.Forms.Button();
            this.Radio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Genre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Web = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Url = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pb1 = new System.Windows.Forms.ProgressBar();
            this.btnGoogle2 = new System.Windows.Forms.Button();
            this.btnYoutube2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgRadios)).BeginInit();
            this.tc1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBookmarks)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgFavorties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtSearch.ForeColor = System.Drawing.Color.SlateGray;
            this.txtSearch.Location = new System.Drawing.Point(8, 14);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(168, 26);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.Text = "Search";
            this.txtSearch.Click += new System.EventHandler(this.txtSearch_Click);
            // 
            // dgRadios
            // 
            this.dgRadios.AllowUserToAddRows = false;
            this.dgRadios.AllowUserToDeleteRows = false;
            this.dgRadios.AllowUserToOrderColumns = true;
            this.dgRadios.AllowUserToResizeRows = false;
            this.dgRadios.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgRadios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgRadios.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgRadios.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgRadios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRadios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Radio,
            this.Genre,
            this.Web,
            this.Url});
            this.dgRadios.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgRadios.GridColor = System.Drawing.Color.WhiteSmoke;
            this.dgRadios.Location = new System.Drawing.Point(4, 58);
            this.dgRadios.Name = "dgRadios";
            this.dgRadios.RowHeadersVisible = false;
            this.dgRadios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgRadios.Size = new System.Drawing.Size(617, 333);
            this.dgRadios.TabIndex = 2;
            this.dgRadios.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgRadios_CellContentClick);
            this.dgRadios.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgRadios_CellMouseClick);
            this.dgRadios.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgRadios_CellMouseDoubleClick);
            // 
            // tc1
            // 
            this.tc1.Controls.Add(this.tabPage1);
            this.tc1.Controls.Add(this.tabPage2);
            this.tc1.Controls.Add(this.tabPage3);
            this.tc1.Controls.Add(this.tabPage4);
            this.tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tc1.Location = new System.Drawing.Point(0, 0);
            this.tc1.Name = "tc1";
            this.tc1.SelectedIndex = 0;
            this.tc1.Size = new System.Drawing.Size(632, 421);
            this.tc1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pb1);
            this.tabPage1.Controls.Add(this.btnAddToFavorites);
            this.tabPage1.Controls.Add(this.btnSearch);
            this.tabPage1.Controls.Add(this.dgRadios);
            this.tabPage1.Controls.Add(this.txtSearch);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(624, 395);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Search radios";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnAddToFavorites
            // 
            this.btnAddToFavorites.Location = new System.Drawing.Point(500, 14);
            this.btnAddToFavorites.Name = "btnAddToFavorites";
            this.btnAddToFavorites.Size = new System.Drawing.Size(115, 26);
            this.btnAddToFavorites.TabIndex = 6;
            this.btnAddToFavorites.Text = "Add to favorites";
            this.btnAddToFavorites.UseVisualStyleBackColor = true;
            this.btnAddToFavorites.Click += new System.EventHandler(this.btnAddToFavorites_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(182, 14);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 26);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnRemoveFromFavorties);
            this.tabPage2.Controls.Add(this.dgFavorties);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(624, 395);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Favorites";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnSearchGoogle);
            this.tabPage3.Controls.Add(this.btnSearchYoutube);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.dgBookmarks);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(624, 395);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Bookmarks";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnSearchGoogle
            // 
            this.btnSearchGoogle.Location = new System.Drawing.Point(395, 16);
            this.btnSearchGoogle.Name = "btnSearchGoogle";
            this.btnSearchGoogle.Size = new System.Drawing.Size(110, 23);
            this.btnSearchGoogle.TabIndex = 4;
            this.btnSearchGoogle.Text = "Search google mp3";
            this.btnSearchGoogle.UseVisualStyleBackColor = true;
            this.btnSearchGoogle.Click += new System.EventHandler(this.btnSearchGoogle_Click);
            // 
            // btnSearchYoutube
            // 
            this.btnSearchYoutube.Location = new System.Drawing.Point(511, 16);
            this.btnSearchYoutube.Name = "btnSearchYoutube";
            this.btnSearchYoutube.Size = new System.Drawing.Size(110, 23);
            this.btnSearchYoutube.TabIndex = 4;
            this.btnSearchYoutube.Text = "Search youtube";
            this.btnSearchYoutube.UseVisualStyleBackColor = true;
            this.btnSearchYoutube.Click += new System.EventHandler(this.btnSearchYoutube_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(4, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Remove";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // dgBookmarks
            // 
            this.dgBookmarks.AllowUserToAddRows = false;
            this.dgBookmarks.AllowUserToDeleteRows = false;
            this.dgBookmarks.AllowUserToOrderColumns = true;
            this.dgBookmarks.AllowUserToResizeRows = false;
            this.dgBookmarks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgBookmarks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgBookmarks.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgBookmarks.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgBookmarks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBookmarks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SongName,
            this.cRadio,
            this.DateAdded});
            this.dgBookmarks.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgBookmarks.GridColor = System.Drawing.Color.WhiteSmoke;
            this.dgBookmarks.Location = new System.Drawing.Point(4, 55);
            this.dgBookmarks.Name = "dgBookmarks";
            this.dgBookmarks.RowHeadersVisible = false;
            this.dgBookmarks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgBookmarks.Size = new System.Drawing.Size(617, 333);
            this.dgBookmarks.TabIndex = 3;
            // 
            // SongName
            // 
            this.SongName.HeaderText = "Song name";
            this.SongName.Name = "SongName";
            // 
            // cRadio
            // 
            this.cRadio.HeaderText = "Radio";
            this.cRadio.Name = "cRadio";
            // 
            // DateAdded
            // 
            dataGridViewCellStyle3.Format = "g";
            dataGridViewCellStyle3.NullValue = null;
            this.DateAdded.DefaultCellStyle = dataGridViewCellStyle3;
            this.DateAdded.HeaderText = "Date added";
            this.DateAdded.Name = "DateAdded";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btnGoogle2);
            this.tabPage4.Controls.Add(this.btnYoutube2);
            this.tabPage4.Controls.Add(this.dgHistory);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(624, 395);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Song history";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dgHistory
            // 
            this.dgHistory.AllowUserToAddRows = false;
            this.dgHistory.AllowUserToDeleteRows = false;
            this.dgHistory.AllowUserToOrderColumns = true;
            this.dgHistory.AllowUserToResizeRows = false;
            this.dgHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgHistory.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgHistory.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.dgHistory.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgHistory.GridColor = System.Drawing.Color.WhiteSmoke;
            this.dgHistory.Location = new System.Drawing.Point(4, 55);
            this.dgHistory.Name = "dgHistory";
            this.dgHistory.RowHeadersVisible = false;
            this.dgHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgHistory.Size = new System.Drawing.Size(617, 333);
            this.dgHistory.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Song name";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Radio";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle4.Format = "g";
            dataGridViewCellStyle4.NullValue = null;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn3.HeaderText = "Date added";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dgFavorties
            // 
            this.dgFavorties.AllowUserToAddRows = false;
            this.dgFavorties.AllowUserToDeleteRows = false;
            this.dgFavorties.AllowUserToOrderColumns = true;
            this.dgFavorties.AllowUserToResizeRows = false;
            this.dgFavorties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgFavorties.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgFavorties.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgFavorties.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgFavorties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFavorties.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.dgFavorties.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgFavorties.GridColor = System.Drawing.Color.WhiteSmoke;
            this.dgFavorties.Location = new System.Drawing.Point(3, 59);
            this.dgFavorties.Name = "dgFavorties";
            this.dgFavorties.RowHeadersVisible = false;
            this.dgFavorties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgFavorties.Size = new System.Drawing.Size(617, 333);
            this.dgFavorties.TabIndex = 3;
            this.dgFavorties.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgFavorties_CellMouseClick);
            this.dgFavorties.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgRadios_CellMouseDoubleClick);
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Radio";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Genre";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Web";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Url";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // btnRemoveFromFavorties
            // 
            this.btnRemoveFromFavorties.Location = new System.Drawing.Point(3, 17);
            this.btnRemoveFromFavorties.Name = "btnRemoveFromFavorties";
            this.btnRemoveFromFavorties.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveFromFavorties.TabIndex = 4;
            this.btnRemoveFromFavorties.Text = "Remove";
            this.btnRemoveFromFavorties.UseVisualStyleBackColor = true;
            this.btnRemoveFromFavorties.Click += new System.EventHandler(this.btnRemoveFromFavorties_Click);
            // 
            // Radio
            // 
            this.Radio.HeaderText = "Radio";
            this.Radio.Name = "Radio";
            // 
            // Genre
            // 
            this.Genre.HeaderText = "Genre";
            this.Genre.Name = "Genre";
            this.Genre.Visible = false;
            // 
            // Web
            // 
            this.Web.HeaderText = "Web";
            this.Web.Name = "Web";
            // 
            // Url
            // 
            this.Url.HeaderText = "Url";
            this.Url.Name = "Url";
            this.Url.Visible = false;
            // 
            // pb1
            // 
            this.pb1.Enabled = false;
            this.pb1.Location = new System.Drawing.Point(263, 14);
            this.pb1.MarqueeAnimationSpeed = 50;
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(231, 26);
            this.pb1.TabIndex = 7;
            // 
            // btnGoogle2
            // 
            this.btnGoogle2.Location = new System.Drawing.Point(395, 16);
            this.btnGoogle2.Name = "btnGoogle2";
            this.btnGoogle2.Size = new System.Drawing.Size(110, 23);
            this.btnGoogle2.TabIndex = 5;
            this.btnGoogle2.Text = "Search google mp3";
            this.btnGoogle2.UseVisualStyleBackColor = true;
            this.btnGoogle2.Click += new System.EventHandler(this.btnGoogle2_Click);
            // 
            // btnYoutube2
            // 
            this.btnYoutube2.Location = new System.Drawing.Point(511, 16);
            this.btnYoutube2.Name = "btnYoutube2";
            this.btnYoutube2.Size = new System.Drawing.Size(110, 23);
            this.btnYoutube2.TabIndex = 6;
            this.btnYoutube2.Text = "Search youtube";
            this.btnYoutube2.UseVisualStyleBackColor = true;
            this.btnYoutube2.Click += new System.EventHandler(this.btnYoutube2_Click);
            // 
            // frmSearchRadios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.ClientSize = new System.Drawing.Size(632, 421);
            this.Controls.Add(this.tc1);
            this.Name = "frmSearchRadios";
            this.ShowInTaskbar = false;
            this.Text = "frmSearchRadios";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSearchRadios_FormClosing);
            this.Load += new System.EventHandler(this.frmSearchRadios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgRadios)).EndInit();
            this.tc1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgBookmarks)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgFavorties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgRadios;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnAddToFavorites;
        private System.Windows.Forms.DataGridView dgBookmarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn SongName;
        private System.Windows.Forms.DataGridViewTextBoxColumn cRadio;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateAdded;
        private System.Windows.Forms.Button btnSearchGoogle;
        private System.Windows.Forms.Button btnSearchYoutube;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dgHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button btnRemoveFromFavorties;
        private System.Windows.Forms.DataGridView dgFavorties;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Radio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Genre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Web;
        private System.Windows.Forms.DataGridViewTextBoxColumn Url;
        private System.Windows.Forms.ProgressBar pb1;
        public System.Windows.Forms.TabControl tc1;
        public System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnGoogle2;
        private System.Windows.Forms.Button btnYoutube2;
    }
}