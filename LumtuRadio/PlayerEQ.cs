﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Fx;

namespace LumtuRadio
{
    public static partial class Player
    {

        private static int _fxEQ;
      


        private static void Set_EQ(int channel)
        {
          
          _fxEQ = Bass.BASS_ChannelSetFX(channel, BASSFXType.BASS_FX_BFX_PEAKEQ, 0);

          BASS_BFX_PEAKEQ eq = new BASS_BFX_PEAKEQ();
          eq.fQ = 0f;
          eq.fBandwidth = 2.5f;
          eq.lChannel = BASSFXChan.BASS_BFX_CHANALL;
   
          eq.lBand = 0;
          eq.fCenter = 70f;
          Bass.BASS_FXSetParameters(_fxEQ, eq);
          UpdateEQ(eq.lBand);

          eq.lBand = 1;
          eq.fCenter = 180f;
          Bass.BASS_FXSetParameters(_fxEQ, eq);
          UpdateEQ(eq.lBand);

          eq.lBand = 2;
          eq.fCenter = 320f;
          Bass.BASS_FXSetParameters(_fxEQ, eq);
          UpdateEQ(eq.lBand);

          eq.lBand = 3;
          eq.fCenter = 600f;
          Bass.BASS_FXSetParameters(_fxEQ, eq);
          UpdateEQ(eq.lBand);

          eq.lBand = 4;
          eq.fCenter = 1000;
          Bass.BASS_FXSetParameters(_fxEQ, eq);
          UpdateEQ(eq.lBand);

          eq.lBand = 5;
          eq.fCenter = 3000f;
          Bass.BASS_FXSetParameters(_fxEQ, eq);
          UpdateEQ(eq.lBand);

          eq.lBand = 6;
          eq.fCenter = 6000f;
          Bass.BASS_FXSetParameters(_fxEQ, eq);
          UpdateEQ(eq.lBand);

          eq.lBand = 7;
          eq.fCenter = 12000f;
          Bass.BASS_FXSetParameters(_fxEQ, eq);
          UpdateEQ(eq.lBand);

          eq.lBand = 8;
          eq.fCenter = 14000f;
          Bass.BASS_FXSetParameters(_fxEQ, eq);
          UpdateEQ(eq.lBand);

          eq.lBand = 9;
          eq.fCenter = 16000f;
          Bass.BASS_FXSetParameters(_fxEQ, eq);
          UpdateEQ(eq.lBand);

         
        }

        public static int[] EQBandValue;
       

        public static void UpdateEQ(int band )
        {
           
          BASS_BFX_PEAKEQ eq = new BASS_BFX_PEAKEQ();
          // get values of the selected band
          eq.lBand = band;
          Bass.BASS_FXGetParameters(_fxEQ, eq);
          eq.fGain = EQBandValue[band];
            
          Bass.BASS_FXSetParameters(_fxEQ, eq);
        }
    }
}
