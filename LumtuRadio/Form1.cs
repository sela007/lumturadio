﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace LumtuRadio
{
    public partial class Form1 : Form
    {


        #region MicanjeForme
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture(); 
        #endregion

        public Form1()
        {
            InitializeComponent();
        }

        public Settings settings;
        frmSearchRadios frmR;
        private void btnGetRadios_Click(object sender, EventArgs e)
        {
            frmR.tc1.SelectTab(0);
            frmR.Show();
            lblRadio.Focus();
  
        }

        private void tViz_Tick(object sender, EventArgs e)
        {
            if (Player.State == Player.PlayerState.Streaming)
            {
                pbViz.BackgroundImage = Player.GetSpectrum(new Size (pbViz.Width ,pbViz.Height ) );

            }
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            settings = Settings.Load();
            frmR = new frmSearchRadios(settings,new frmSearchRadios.delPlay (Play ) );
            cmFavorites.ItemClicked += cmFavorites_ItemClicked;
            Player.FileChanged += Player_FileChanged;
            //Player.StreamInfoCompleted += Player_StreamInfoCompleted;
            Player.StreamInfoChanged += Player_StreamInfoChanged;
            Player.SpectrumColor1 = Color.WhiteSmoke;
            Player.StateChanged += Player_StateChanged;
            
            pbViz.MouseDown += pbViz_MouseDown;
            frmR.RefreshBookmarks();
            frmR.LoadHistory();
            frmR.RefreshFavorites();
            settings.Color1 = Color.WhiteSmoke;
            SetColor1(settings.Color1);
            mnuItmStayOnTop.Checked = settings.StayOnTop;
            this.TopMost = settings.StayOnTop;
            this.Location = settings.FormPosition;

            //kada se ugasi drugi monitor vrati na prvi,ako je bio na drugom
            if (Screen.AllScreens.Count() == 1)
            {
                if (this.Location.X >= Screen.PrimaryScreen.WorkingArea.Width) this.Left  = 100;
                if (this.Location.Y >= Screen.PrimaryScreen.WorkingArea.Height ) this.Top = 100;

            }
            Player.Volume = settings.Volume;
            SetVolume(Player.Volume);
            
        }

        void Player_StreamInfoChanged(PlayerEventArgs e)
        {
            lblSong.Text = Player.CurrentStream.CurrentSongAuthor + " - " + Player.CurrentStream.CurrentSongTitle;
            Bookmark b = new Bookmark
            {
                Name = Player.CurrentStream.CurrentSongAuthor + " - " + Player.CurrentStream.CurrentSongTitle,
                Radio = Player.CurrentRadio.Name,
                DateAdded = DateTime.Now
            };
            settings.AddToHistory(b);
            frmR.AddToHistory(b);
        }

        private void PaintButton (Button btn,Color c)
        {
            Bitmap b = (Bitmap)btn.Image;
            for (int x = 0; x <= b.Width-1; x++)
            {
                for (int y = 0; y <= b.Height-1 ; y++)
                {
                    Color pixel = b.GetPixel(x, y);
                    b.SetPixel(x, y, Color.FromArgb(pixel.A ,c.R ,c.G,c.B )  );
                }
            }

        }

        private void SetColor1(Color c)
        {

            PaintButton(btnBookmark, c);
            PaintButton(btnClose , c);
            PaintButton(btnMinimize, c);
            PaintButton(btnPlay, c);
            PaintButton(btnSearch, c);
            PaintButton(btnFavorites, c);
            PaintButton(btnEq, c);
            PaintButton(btnSettings, c);
            lblRadio.ForeColor = c;
            lblSong.ForeColor = c;

        }


        void pbViz_MouseDown(object sender, MouseEventArgs e)
        {
             
        }

        void Player_StateChanged(PlayerEventArgs e)
        {
            if (e.State == Player.PlayerState.ServerUnreachable)
            {
                lblSong.Text = "Server unreachable...";
             
            }

            if (e.State == Player.PlayerState.Streaming)
            {
                lblSong.Text = "Loading information...";
                btnPlay.Image = Properties.Resources.pause_3_32;
            }
            else
            {
                 btnPlay.Image = Properties.Resources.play_3_32;
            }

           
        }

        void Player_StreamInfoCompleted(PlayerEventArgs e)
        {
              

        }

        void Player_FileChanged(PlayerEventArgs e)
        {
            lblRadio.Text = Player.CurrentRadio.Name;
        }

        void cmFavorites_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            Play(settings.FavoriteRadios [Convert.ToInt16 (  e.ClickedItem.Tag)  ]);
        }

        private void Play(Radio radio)
        {
            lblSong.Text = "Loading...";
            Player.Play(radio);
        }

     

       

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            settings.Volume = Player.Volume;
            settings.FormPosition = this.Location;
            settings.CurrentRadio = Player.CurrentRadio;
            settings.Save();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Player.State == Player.PlayerState.Streaming)
            {
                Player.UpdateCurrentStreamInfo();
            }
            lblSong.Text = Player.CurrentStream.CurrentSongAuthor + " - " + Player.CurrentStream.CurrentSongTitle;
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();


        }

        private void btnFavorites_Click(object sender, EventArgs e)
        {
            cmFavorites.Items.Clear();

            int i = -1;
            foreach (Radio r in settings.FavoriteRadios)
            {
                i++;
                cmFavorites.Items.Add(r.Name);
                cmFavorites.Items[cmFavorites.Items.Count - 1].Tag = i;
                cmFavorites.Show(Cursor.Position);
            }
            lblRadio.Focus();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            lblRadio.Focus();

        }

        private void pbVol_MouseDown(object sender, MouseEventArgs e)
        {

            SetVolume(e.X);

        }


        private void SetVolume(int x)
        {
            Color color = settings.Color1;
            Pen DarkPen = new Pen(Color.DarkGray, 1);
            Pen MidPen = new Pen(Color.FromArgb(40, color), 1);
            Pen LightPen = new Pen(Color.FromArgb(170, color), 1);
            int dur = x;
            if (dur < 0) dur = 0;
            if (dur > 100) dur = 100;

            Bitmap b = new Bitmap(100, 5);
            using (Graphics g = Graphics.FromImage(b))
            {
                g.DrawLine(DarkPen, 1, 1, 100, 1);
                g.DrawLine(DarkPen, 1, 1, 1, 5);
                g.FillRectangle(new SolidBrush(MidPen.Color), dur + 1, 2, 100, 3);
                g.FillRectangle(new SolidBrush(LightPen.Color), 2, 2, dur, 3);
                g.DrawLine(LightPen, 2, 5, 100, 5);
                g.DrawLine(DarkPen, 100, 2, 100, 5);

            }
            pbVol.BackgroundImage = b;
            Player.Volume = x;

        }
        private void pbVol_Click(object sender, EventArgs e)
        {
            
        }

        private void pbVol_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                SetVolume(e.X);
            }
        }

        private void lblSong_TextChanged(object sender, EventArgs e)
        {
            //premjesteno u Player.StreamInfoChanged event 

            //if (lblSong.Text.Trim () == "") return;
            //if (lblSong.Text == "Loading...") return;
            //if (lblSong.Text == "Loading information...") return;
            //if (lblSong.Text == "Server unreachable...") return;
            //Bookmark b = new Bookmark
            //{
            //    Name = Player.CurrentStream.CurrentSongAuthor + " - " + Player.CurrentStream.CurrentSongTitle,
            //    Radio = Player.CurrentRadio.Name,
            //    DateAdded = DateTime.Now
            //};
            //settings.AddToHistory(b);
            //frmR.AddToHistory (b);
           

        }

        private void pbViz_Click(object sender, EventArgs e)
        {
            
        }

        private void pbViz_MouseUp(object sender, MouseEventArgs e)
        {
            Player.NextViz();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            Player.PlayPause();
            lblRadio.Focus();
        }

        private void moveToBottomRightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Top = Screen.PrimaryScreen.WorkingArea.Height - this.Height;
            this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width;
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {   lblRadio.Focus();
            cmSettings.Show(Cursor.Position);
            
        }

        private void btnBookmark_Click(object sender, EventArgs e)
        {
            lblSong.Text = "Bookmarked!";
            timer1.Enabled = false;
            timer1.Enabled = true;
            AddBookmark();
            Player.UpdateCurrentStreamInfo();
            lblRadio.Focus();
        }

        private void AddBookmark()
        {
            Bookmark b = new Bookmark();
            b.Name = Player.CurrentStream.CurrentSongAuthor + " - " + Player.CurrentStream.CurrentSongTitle;
            b.Radio = Player.CurrentRadio.Name;
            b.DateAdded = DateTime.Now;
            settings.Bookmarks.Add(b);
            frmR.AddBookmark(b);

        }

        private void cmFavorites_Opening(object sender, CancelEventArgs e)
        {

        }

        private void lblSong_Click(object sender, EventArgs e)
        {
            
        }

        private void stayOnTopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mnuItmStayOnTop.Checked = Func.Reverse(mnuItmStayOnTop.Checked);
            this.TopMost = mnuItmStayOnTop.Checked;
            settings.StayOnTop = mnuItmStayOnTop.Checked;

        }

        private void addRadioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAddRadio frm = new frmAddRadio();
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Radio r = new Radio();
                r.Name = frm.txtName.Text;
                r.Genre = frm.txtGenre.Text;
                r.Web = frm.txtWeb.Text;
                r.Url = frm.txtUrl.Text;
                settings.FavoriteRadios.Add(r);
                frmR.RefreshFavorites();

            }
        }

        private void bookmarksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmR.tc1.SelectTab(2);
            frmR.Show();
        }

        private void searchRadiosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmR.tc1.SelectTab(0);
            frmR.txtSearch.Focus();
            frmR.txtSearch.SelectAll();
            frmR.Show();
        }

        private void favoritesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmR.tc1.SelectTab(1);
            frmR.Show();
        }

        private void songHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmR.tc1.SelectTab(3);
            frmR.Show();
        }
    }
}
